package config;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class ReadProperties {

    static Properties prop = new Properties();
    static String configPath = System.getProperty("user.dir");
    public static String browser = null;
    public static int timeout = 10;

    /**
     * Get and Set Properties from config.properties file
     */
    public static void getProperties(){
        try {
            InputStream input = new FileInputStream(configPath+"/src/test/java/config/config.properties");
            prop.load(input);
            browser = prop.getProperty("browser").replaceAll("\\s", "");
            System.out.println("Browser: " + browser);
            timeout =  Integer.parseInt(prop.getProperty("timeout"));
            System.out.println("Timeout: "+ timeout);

        }catch(Exception exp) {
            System.out.println(exp.getMessage());
            System.out.println(exp.getCause());
            exp.printStackTrace();
        }
    }

}
