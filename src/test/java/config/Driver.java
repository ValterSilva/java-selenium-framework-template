package config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.util.concurrent.TimeUnit;

public class Driver {

    /**
     * Generates WebDriver according to config variables (browser and timeout)
     *
     * @return WebDriver
     */
    public static WebDriver getDriver() {
        WebDriver driver = null;
        String projectPath = System.getProperty("user.dir");
        ReadProperties.getProperties();

        if (ReadProperties.browser.equalsIgnoreCase("chrome")) {
            //Set chrome driver path
            projectPath += "\\drivers\\chromedriver\\chromedriver.exe";
            System.setProperty("webdriver.chrome.driver", projectPath);
            System.out.println("projectPath: " + projectPath);

            //Create chrome driver options profile
            ChromeOptions options = new ChromeOptions();
            options.setCapability("profile.default_content_setting_values.cookies", 2);

            //Create chrome driver and set timeout value
            driver = new ChromeDriver(options);
            driver.manage().timeouts().implicitlyWait(ReadProperties.timeout, TimeUnit.SECONDS);

        } else if (ReadProperties.browser.equalsIgnoreCase("firefox")) {
            //Set firefox driver path
            projectPath += "\\drivers\\geckodriver\\geckodriver.exe";
            System.out.println("projectPath: " + projectPath);
            System.setProperty("webdriver.gecko.driver", projectPath);

            //Create firefox driver options profile
            FirefoxProfile profile = new FirefoxProfile();
            profile.setPreference("network.cookie.cookieBehavior", 2);
            FirefoxOptions options = new FirefoxOptions();
            options.setProfile(profile);

            //Create firefox driver and set timeout value
            driver = new FirefoxDriver(options);
            driver.manage().timeouts().implicitlyWait(ReadProperties.timeout, TimeUnit.SECONDS);
        }

        return driver;
    }
}
