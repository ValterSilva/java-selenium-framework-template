package testScripts;

import config.Driver;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import pageModels.GoogleMapsPage;

import static org.junit.Assert.assertTrue;

@RunWith(JUnitParamsRunner.class)
public class GoogleMapsSearch {

    WebDriver driver = Driver.getDriver();
    GoogleMapsPage googleMapsPage;

    @Before
    public void setUpTest() {
        this.driver.manage().window().maximize();
        this.driver.get("https://www.google.com/maps/");
    }

    @Test
    @DisplayName("Search location and verify direction search input")
    @Parameters({"Dublin", "Porto"})
    public void performSearchInGoogleMaps(String search) {
        this.googleMapsPage = new GoogleMapsPage(this.driver);

        //Perform google maps search and validate the result headliner is equal to the search value
        this.googleMapsPage.search(search);
        assertTrue("Headliner title does not correspond to the search",
                this.googleMapsPage.getHeadliner().equals(search));

        //Click in the Directions button and validate the destination input contains the search value
        this.googleMapsPage.clickDirectionsButton();
        assertTrue("Destination input box does not contain search value",
                this.googleMapsPage.getDestinationTextInput().contains(search));
    }

    @After
    public void cleanUpTest() {
        if (this.driver != null)
            this.driver.quit();
    }
}
