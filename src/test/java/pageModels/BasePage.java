package pageModels;

import config.ReadProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {
    public WebDriver driver;
    public Actions action;
    
    public void waitForElement(WebElement element) {
        new WebDriverWait(driver, ReadProperties.timeout)
                .until(ExpectedConditions.visibilityOf(element));
    }
}