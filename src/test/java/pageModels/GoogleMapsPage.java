package pageModels;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleMapsPage extends BasePage {

    @FindBy(xpath = "//*[@id='searchboxinput']")
    WebElement searchBoxInput;

    @FindBy(xpath = "//*[@id='pane']/div/div[1]/div/div/div[2]/div[1]/div[1]/div[1]/h1/span[1]")
    WebElement searchHeadline;

    @FindBy(xpath = "//*[@id='pane']/div/div[1]/div/div/div[4]/div[1]/div/button")
    WebElement directionsButton;

    @FindBy(xpath = "//*[@id='sb_ifc52']/input")
    WebElement destinationTextInput;

    public GoogleMapsPage(WebDriver driver) {
        this.driver = driver;
        this.action = new Actions(driver);
        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
    }

    //Insert text search and press Enter in the search box input
    public void search(String text) {
        this.waitForElement(searchBoxInput);
        searchBoxInput.sendKeys(text);
        searchBoxInput.sendKeys(Keys.ENTER);
    }

    //Click in Directions Button
    public void clickDirectionsButton() {
        directionsButton.click();
    }

    //Get Title from search Headliner
    public String getHeadliner() {
        this.waitForElement(searchHeadline);
        return searchHeadline.getText();
    }

    //Get Text from destination text input
    public String getDestinationTextInput() {
        this.waitForElement(destinationTextInput);
        return destinationTextInput.getAttribute("aria-label");
    }
}
