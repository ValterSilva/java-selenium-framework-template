# Java_Selenium_Framework 
Java selenium framework template with JUnit and Page Object Model (POM) using Selenium Page Factory

## Dependencies
* Java
* Maven

## Libraries used
* jUnit 4.13.2
* selenium-java 4.0.0-beta-4
* JUnitParams 1.1.1
* slf4j-api 1.8.0-beta2

## Steps to execute tests
1. git clone https://ValterSilva@bitbucket.org/ValterSilva/java-selenium-framework-template.git
1. Build Maven dependencies
1. Run Test from src/test/java/testScripts/GoogleMapsSearch.java file


## Project Structure
* drivers > contains both selenium drivers .exe for google and firefox
* src/test/java/pageModels > [PageObjectModel (POM)](https://www.browserstack.com/guide/page-object-model-in-selenium) structure
* src/test/java/settings > contains the configuration properties and the class to Load/Store its values
* src/test/java/testScripts > JUnit script with test example using junit Assertions

